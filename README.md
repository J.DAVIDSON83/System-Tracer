# System Tracer

A program to monitor user activity on a system.

## About
You may be a sysadmin or have to let someone you don't know that well use your computer. (For example, when you have to send your computer for repairs.) No matter the reason, there may be times when you want to keep a close eye on what a user is doing on your system. *System Tracer* has been designed for just such circumstances. By keeping logs of what another user is doing on your computer, it attempts to give you the peace of mind that your computer or your data has not been compromised.

## Monitoring Capabilities
- Connected Storage Devices
- Webcam Photos
- Screenshots

## License
This project is licensed under **GPL v3 or later**.