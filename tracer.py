#!/usr/bin/python3

from modules.device_monitor import monitor_devices
from modules.webcam import take_photo
from modules.screenshot import take_screenshot
from datetime import datetime
import getpass
import time
import notify2
import os


def show_notification():
    """Notify the user of program startup."""
    notify2.init('Tracer')
    n = notify2.Notification('Tracer Is Active!', 'You are being monitored.')
    n.show()


def main():
    """Start the main loop."""
    if not os.path.exists(save_folder):
        os.mkdir(save_folder)

    while True:
        timestamp = datetime.now()
        take_screenshot(save_folder, timestamp)
        monitor_devices(save_folder, timestamp)
        take_photo(save_folder, timestamp)
        time.sleep(60)


if __name__ == '__main__':
    save_folder = f'/home/{getpass.getuser()}/.tracer'
    show_notification()
    main()
