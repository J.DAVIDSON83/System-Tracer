import os
import cv2


def take_photo(save_folder, timestamp):
    """If a webcam is connected, take a photo of the user's face."""
    location = f'{save_folder}/webcam'
    # Organize the photos by date and time.
    subfolder = f'{location}/{timestamp.strftime("%d-%m-%Y")}'
    filename = f'{subfolder}/{timestamp.strftime("%X")}.png'

    camera = cv2.VideoCapture(0)
    # Give a 30 frames pause for the camera to adjust.
    ramp_frames = 30

    # Check if a webcam is connected.
    if camera.isOpened():
        for i in range(ramp_frames):
            camera.read()[1]
        if not os.path.exists(subfolder):
            os.makedirs(subfolder)
        # Take the photo.
        capture = camera.read()[1]
        cv2.imwrite(filename, capture)
        del camera
