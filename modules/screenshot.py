import os
import pyautogui


def take_screenshot(save_folder, timestamp):
    """Take a screenshot of the desktop."""
    location = f'{save_folder}/screenshots'
    # Organize the screenshots by date and time.
    subfolder = f'{location}/{timestamp.strftime("%d-%m-%Y")}'
    filename = f'{subfolder}/{timestamp.strftime("%X")}.png'

    if not os.path.exists(subfolder):
        os.makedirs(subfolder)

    screenshot = pyautogui.screenshot()
    screenshot.save(filename)
