import os
import codecs
import logging
import csv

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logging.disable(logging.CRITICAL)


def monitor_devices(save_folder, timestamp):
    """List the contents of any mounted external drives, if any."""
    device_directory = '/dev/disk/by-label'
    device_labels = get_device_labels(device_directory)
    mounts_file = '/proc/mounts'
    device_folder = os.path.join(save_folder, 'devices')

    if device_labels:
        devices = get_connected_devices(device_directory, device_labels)
        logging.info(f'devices = {devices}')
        mountpoints = get_mounted_device_locations(mounts_file, devices)
        if mountpoints:
            log_path = get_log_path(device_folder, timestamp)
            generate_logs(log_path, mountpoints)


def get_connected_devices(device_directory, device_labels):
    devices = []
    for label in device_labels:
        path = os.path.join(device_directory, label)
        device = os.path.basename(os.readlink(path))
        device = os.path.join('/dev', device)
        devices.append(device)

    return devices


def get_mounted_device_locations(mounts_file, devices):
    with open(mounts_file) as f:
        mounts = f.readlines()

    mountpoints = {}

    for device in devices:
        for mount in mounts:
            mounted_device = mount.split()[0]
            mounted_location = mount.split()[1]

            if device == mounted_device:
                location = codecs.unicode_escape_decode(mounted_location)[0]
                mountpoints[device] = location

    return mountpoints


def get_log_path(device_folder, timestamp):
    folder_path = os.path.join(device_folder, timestamp.strftime('%d-%m-%y'))
    filepath = os.path.join(
        folder_path, f"{timestamp.strftime('%X')}.csv"
    )
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    return filepath


def generate_logs(log_path, mountpoints):
    with open(log_path, 'w') as f:
        log = csv.writer(f)
        for device, location in mountpoints.items():
            device_contents = os.listdir(location)
            log.writerow([device, location, device_contents])


def get_device_labels(device_directory):
    logging.info('Getting device labels...')
    try:
        device_labels = os.listdir(device_directory)
    except FileNotFoundError:
        device_labels = []

    return device_labels
